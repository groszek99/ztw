## Adding migration

```
dotnet ef migrations add <<migration-name>>  --project ./src/Database/Logistics.Database.csproj --context LogisticsDbContext --startup-project ./src/Launcher/Logistics.Launcher.csproj
```

## Updating Database

```
dotnet ef database update --project ./src/Database/Logistics.Database.csproj --context LogisticsDbContext --startup-project ./src/Launcher/Logistics.Launcher.csproj
```