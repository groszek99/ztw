using Logistics.Core.InterestPoints;
using Logistics.Core.Routes;
using Microsoft.EntityFrameworkCore;

namespace Logistics.Database;

internal sealed class LogisticsDbContext : DbContext {
  public const string Schema = "logistics";

  public LogisticsDbContext(DbContextOptions<LogisticsDbContext> options) : base(options) { }

  protected override void OnModelCreating(ModelBuilder modelBuilder) {
    base.OnModelCreating(modelBuilder);

    modelBuilder.HasDefaultSchema(Schema);

    modelBuilder.Entity<InterestPoint>(entity => {
      entity.ToTable("InterestPoints");

      entity.HasKey(point => point.Identifier);
      entity.OwnsOne(point => point.Address);
      entity.OwnsOne(point => point.Coordinates);

      entity.HasOne(point => point.Route)
        .WithMany(point => point.InterestPoints)
        .HasForeignKey("RouteIdentifier");
    });

    modelBuilder.Entity<Route>(entity => {
      entity.ToTable("Routes");

      entity.HasKey(route => route.Identifier);
    });
  }
}