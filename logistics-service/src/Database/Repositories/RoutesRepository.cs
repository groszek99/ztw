using Logistics.Core.InterestPoints;
using Logistics.Database.Abstraction;
using Microsoft.EntityFrameworkCore;

namespace Logistics.Database.Repositories;

public class RoutesRepository : AsyncDatabaseRepository<long, InterestPoint>, IInterestPointsRepository {
  public RoutesRepository(DbContext dbContext) : base(dbContext) { }
}