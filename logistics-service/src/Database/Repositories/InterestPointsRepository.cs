using Logistics.Core.InterestPoints;
using Logistics.Database.Abstraction;
using Microsoft.EntityFrameworkCore;

namespace Logistics.Database.Repositories;

public class InterestPointsRepository : AsyncDatabaseRepository<long, InterestPoint>, IInterestPointsRepository {
  public InterestPointsRepository(DbContext dbContext) : base(dbContext) { }
}