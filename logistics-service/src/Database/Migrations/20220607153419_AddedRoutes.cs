﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Logistics.Database.Migrations
{
    public partial class AddedRoutes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "Routes",
                schema: "logistics",
                columns: table => new
                {
                    Identifier = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    UserIdentifier = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Routes", x => x.Identifier);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InterestPoints_RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints",
                column: "RouteIdentifier");

            migrationBuilder.AddForeignKey(
                name: "FK_InterestPoints_Routes_RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints",
                column: "RouteIdentifier",
                principalSchema: "logistics",
                principalTable: "Routes",
                principalColumn: "Identifier",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_InterestPoints_Routes_RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints");

            migrationBuilder.DropTable(
                name: "Routes",
                schema: "logistics");

            migrationBuilder.DropIndex(
                name: "IX_InterestPoints_RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints");

            migrationBuilder.DropColumn(
                name: "RouteIdentifier",
                schema: "logistics",
                table: "InterestPoints");
        }
    }
}
