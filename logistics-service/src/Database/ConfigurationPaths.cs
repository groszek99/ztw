namespace Logistics.Database;

internal static class ConfigurationPaths {
  public static class Database {
    public const string Root = "Database";
    public const string Logistics = $"{Root}:Logistics";
  }
}