using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Logistics.Core.Abstraction;
using Microsoft.EntityFrameworkCore;

namespace Logistics.Database.Abstraction;

public abstract class AsyncDatabaseRepository<TKey, TEntity> : IAsyncRepository<TKey, TEntity>
  where TEntity : class {
  private readonly DbContext dbContext;

  protected AsyncDatabaseRepository(DbContext dbContext) => this.dbContext = dbContext;

  public async Task<TEntity?> GetOneAsync(TKey key) =>
    await dbContext
      .Set<TEntity>()
      .FindAsync(key);

  public async Task<IReadOnlyCollection<TEntity>> GetManyAsync() =>
    await dbContext
      .Set<TEntity>()
      .ToListAsync();

  public async Task<IReadOnlyCollection<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate) =>
    await dbContext
      .Set<TEntity>()
      .Where(predicate)
      .ToListAsync();

  public async Task<TEntity> CreateAsync(TEntity entity) {
    var entityEntry = await dbContext
      .Set<TEntity>()
      .AddAsync(entity);
    await dbContext.SaveChangesAsync();

    return entityEntry.Entity;
  }

  public async Task<TEntity> DeleteAsync(TEntity entity) {
    var entityEntry = dbContext
      .Set<TEntity>()
      .Remove(entity);
    await dbContext.SaveChangesAsync();

    return entityEntry.Entity;
  }

  public async Task<TEntity> UpdateAsync(TEntity entity, TEntity values) {
    var entityEntry = dbContext
      .Entry(entity);

    entityEntry.CurrentValues.SetValues(values);
    entityEntry.State = EntityState.Modified;

    await dbContext.SaveChangesAsync();

    return entityEntry.Entity;
  }
}