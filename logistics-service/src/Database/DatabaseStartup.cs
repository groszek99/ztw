using Logistics.Core.InterestPoints;
using Logistics.Database.Configuration;
using Logistics.Database.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Logistics.Database;

public static class DatabaseStartup {
  private static IServiceCollection AddRepositories(this IServiceCollection services) =>
    services.AddScoped<IInterestPointsRepository, InterestPointsRepository>();

  public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration) {
    var config = configuration.GetRequiredSection(ConfigurationPaths.Database.Logistics)
      .Get<DatabaseConfiguration>();

    services.AddDbContext<LogisticsDbContext>(contextOptions =>
      contextOptions
        .UseLazyLoadingProxies()
        .UseNpgsql(config.ConnectionString, npgsqlOptions => {
          npgsqlOptions.MigrationsHistoryTable(config.MigrationsTable, LogisticsDbContext.Schema);
          npgsqlOptions.SetPostgresVersion(config.DatabaseVersion);
        }));
    services.AddScoped<DbContext, LogisticsDbContext>();
    services.AddRepositories();

    return services;
  }

  public static WebApplication UseDatabase(this WebApplication app) {
    var config = app.Configuration.GetRequiredSection(ConfigurationPaths.Database.Logistics)
      .Get<DatabaseConfiguration>();

    if (!config.AutoMigrations) return app;

    using var scope = app.Services.CreateScope();
    var db = scope.ServiceProvider.GetRequiredService<LogisticsDbContext>();

    db.Database.Migrate();

    return app;
  }
}