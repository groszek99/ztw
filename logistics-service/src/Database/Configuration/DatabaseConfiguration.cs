using System;

namespace Logistics.Database.Configuration;

internal class DatabaseConfiguration {
  public string ConnectionString { get; set; }
  public Version DatabaseVersion { get; set; }
  public bool AutoMigrations { get; set; }
  public string MigrationsTable { get; set; }
}