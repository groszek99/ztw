using System;
using Logistics.Api;
using Logistics.Core;
using Logistics.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

Log.Logger = new LoggerConfiguration()
  .MinimumLevel.Debug()
  .Enrich.FromLogContext()
  .WriteTo.Console(
    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{Exception}{NewLine}",
    theme: AnsiConsoleTheme.Literate)
  .CreateBootstrapLogger();

Log.Information("Starting up");

try {
  var builder = WebApplication.CreateBuilder(args);

  builder.Host.UseSerilog((context, services, configuration) =>
    configuration
      .MinimumLevel.Debug()
      .Enrich.FromLogContext()
      .WriteTo.Console(
        outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{Exception}{NewLine}",
        theme: AnsiConsoleTheme.Literate)
      .ReadFrom.Services(services)
      .ReadFrom.Configuration(context.Configuration));

  builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
    config.AddJsonFile("appsettings.local.json", true, true));

  builder.Services.Configure<ForwardedHeadersOptions>(options => { options.ForwardedHeaders = ForwardedHeaders.All; });

  builder.Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer("Bearer", c => {
      c.Authority = $"https://{builder.Configuration["Auth0:Domain"]}";
      c.TokenValidationParameters = new() {
        ValidateAudience = true,
        ValidAudiences = builder.Configuration["Auth0:Audience"]
          .Split(";"),
        ValidateIssuer = true,
        ValidIssuer = $"https://{builder.Configuration["Auth0:Domain"]}",
      };
    });
  builder.Services.AddAuthentication(options => options.RequireAuthenticatedSignIn = true);

  builder.Services.AddHealthChecks();

  builder.Services.AddCore();
  builder.Services.AddDatabase(builder.Configuration);
  builder.Services.AddApi();

  var app = builder.Build();

  app.UseForwardedHeaders();

  if (!app.Environment.IsDevelopment()) {
    app.UseExceptionHandler("/Error");
    app.UseHsts();
  }

  app.UseHttpsRedirection();

  app.UseStaticFiles();

  app.UseRouting();

  app.UseAuthentication();
  app.UseAuthorization();
  app.UseDatabase();
  app.UseApi();

  app.UseEndpoints(configure =>
    configure
      .MapApiEndpoints()
      .MapHealthChecks("/healthz"));

  app.Run();
} catch (Exception ex) {
  Log.Fatal(ex, "Unhandled exception");
} finally {
  Log.Information("Shut down complete");
  Log.CloseAndFlush();
}