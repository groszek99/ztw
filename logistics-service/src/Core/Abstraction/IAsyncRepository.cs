using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Logistics.Core.Abstraction;

public interface IAsyncRepository<in TKey, TEntity>
  where TEntity : class {
  Task<TEntity?> GetOneAsync(TKey key);

  Task<IReadOnlyCollection<TEntity>> GetManyAsync();

  Task<IReadOnlyCollection<TEntity>> GetManyAsync(Expression<Func<TEntity, bool>> predicate);

  Task<TEntity> CreateAsync(TEntity entity);

  Task<TEntity> DeleteAsync(TEntity entity);

  Task<TEntity> UpdateAsync(TEntity entity, TEntity values);
}