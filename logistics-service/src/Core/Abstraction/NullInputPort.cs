namespace Logistics.Core.Abstraction;

public sealed record NullInputPort : IInputPort {
  public static NullInputPort Instance => new();
}