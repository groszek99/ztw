using System.Threading.Tasks;

namespace Logistics.Core.Abstraction;

public interface IUseCase<in TInputPort, in TOutputPort>
  where TInputPort : IInputPort
  where TOutputPort : IOutputPort {
  Task Execute(TInputPort input, TOutputPort output);
}

public interface IUseCase<in TOutputPort> : IUseCase<NullInputPort, TOutputPort>
  where TOutputPort : IOutputPort {
  Task Execute(TOutputPort output) => Execute(NullInputPort.Instance, output);
}