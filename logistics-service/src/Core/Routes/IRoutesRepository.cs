using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes;

public interface IRoutesRepository : IAsyncRepository<long, Route> { }