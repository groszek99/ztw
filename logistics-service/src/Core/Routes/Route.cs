using System.Collections.Generic;
using Logistics.Core.InterestPoints;

namespace Logistics.Core.Routes;

public record Route {
  public long Identifier { get; init; }
  public string Name { get; init; }
  public string UserIdentifier { get; init; }

  public virtual ICollection<InterestPoint> InterestPoints { get; set; }
}