using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes.UseCases;

public class CreateRouteUseCase : IUseCase<CreateRouteUseCase.Input, CreateRouteUseCase.IOutput> {
  private readonly IRoutesRepository routesRepository;

  public CreateRouteUseCase(IRoutesRepository routesRepository) => this.routesRepository = routesRepository;

  public async Task Execute(Input input, IOutput output) {
    var route = await routesRepository.CreateAsync(new() {
      Name = input.Name,
      UserIdentifier = input.UserIdentifier,
    });

    output.Created(route);
  }

  public record Input : IInputPort {
    public string Name { get; init; }
    public string UserIdentifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Created(Route route);

    void OwnerNotFound(Input input);
  }
}