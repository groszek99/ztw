using System.Collections.Generic;
using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes.UseCases;

public class GetManyRoutesUseCase : IUseCase<GetManyRoutesUseCase.IOutput> {
  private readonly IRoutesRepository routesRepository;

  public GetManyRoutesUseCase(IRoutesRepository routesRepository) => this.routesRepository = routesRepository;

  public async Task Execute(NullInputPort input, IOutput output) {
    var route = await routesRepository.GetManyAsync();

    output.Found(route);
  }

  public interface IOutput : IOutputPort {
    void Found(IReadOnlyCollection<Route> routes);
  }
}