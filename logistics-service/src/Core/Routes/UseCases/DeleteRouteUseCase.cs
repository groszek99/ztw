using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes.UseCases;

public class DeleteRouteUseCase : IUseCase<DeleteRouteUseCase.Input, DeleteRouteUseCase.IOutput> {
  private readonly IRoutesRepository routesRepository;

  public DeleteRouteUseCase(IRoutesRepository routesRepository) => this.routesRepository = routesRepository;

  public async Task Execute(Input input, IOutput output) {
    var route = await routesRepository.GetOneAsync(input.Identifier);

    if (route is null) {
      output.NotFound(input);

      return;
    }

    await routesRepository.DeleteAsync(route);

    output.Deleted(route);
  }

  public record Input : IInputPort {
    public long Identifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Deleted(Route route);

    void NotFound(Input input);
  }
}