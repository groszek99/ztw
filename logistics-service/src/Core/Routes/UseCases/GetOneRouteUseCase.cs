using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes.UseCases;

public class GetOneRouteUseCase : IUseCase<GetOneRouteUseCase.Input, GetOneRouteUseCase.IOutput> {
  private readonly IRoutesRepository routesRepository;

  public GetOneRouteUseCase(IRoutesRepository routesRepository) => this.routesRepository = routesRepository;

  public async Task Execute(Input input, IOutput output) {
    var route = await routesRepository.GetOneAsync(input.Identifier);

    if (route is null) {
      output.NotFound(input);

      return;
    }

    output.Found(route);
  }

  public record Input : IInputPort {
    public long Identifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Found(Route route);

    void NotFound(Input input);
  }
}