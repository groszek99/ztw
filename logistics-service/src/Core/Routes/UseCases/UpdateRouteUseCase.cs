using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.Routes.UseCases;

public class UpdateRouteUseCase : IUseCase<UpdateRouteUseCase.Input, UpdateRouteUseCase.IOutput> {
  private readonly IRoutesRepository routesRepository;

  public UpdateRouteUseCase(IRoutesRepository routesRepository) => this.routesRepository = routesRepository;

  public async Task Execute(Input input, IOutput output) {
    var route = await routesRepository.GetOneAsync(input.Identifier);

    if (route is null) {
      output.NotFound(input);

      return;
    }

    var newRoute = await routesRepository.UpdateAsync(route, input);

    output.Updated(newRoute);
  }

  public record Input : Route, IInputPort { }

  public interface IOutput : IOutputPort {
    void Updated(Route route);

    void NotFound(Input input);

    void OwnerNotFound(Input input);
  }
}