using Logistics.Core.Abstraction;

namespace Logistics.Core.InterestPoints;

public interface IInterestPointsRepository : IAsyncRepository<long, InterestPoint> { }