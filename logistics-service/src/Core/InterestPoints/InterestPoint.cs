using Logistics.Core.Common;
using Logistics.Core.Routes;

namespace Logistics.Core.InterestPoints;

public record InterestPoint {
  public long Identifier { get; init; }
  public string Name { get; init; }
  public Address Address { get; init; }
  public Coordinates Coordinates { get; init; }
  public string UserIdentifier { get; init; }

  public virtual Route Route { get; set; }
}