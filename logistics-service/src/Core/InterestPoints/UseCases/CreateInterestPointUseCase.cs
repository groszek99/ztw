using System.Threading.Tasks;
using Logistics.Core.Abstraction;
using Logistics.Core.Common;

namespace Logistics.Core.InterestPoints.UseCases;

public class
  CreateInterestPointUseCase : IUseCase<CreateInterestPointUseCase.Input, CreateInterestPointUseCase.IOutput> {
  private readonly IInterestPointsRepository interestPointsRepository;

  public CreateInterestPointUseCase(IInterestPointsRepository interestPointsRepository) =>
    this.interestPointsRepository = interestPointsRepository;

  public async Task Execute(Input input, IOutput output) {
    var interestPoint = await interestPointsRepository.CreateAsync(new() {
      Name = input.Name,
      Address = input.Address,
      Coordinates = input.Coordinates,
      UserIdentifier = input.UserIdentifier,
    });

    output.Created(interestPoint);
  }

  public record Input : IInputPort {
    public string Name { get; init; }
    public Address Address { get; init; }
    public Coordinates Coordinates { get; init; }
    public string UserIdentifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Created(InterestPoint interestPoint);

    void OwnerNotFound(Input input);
  }
}