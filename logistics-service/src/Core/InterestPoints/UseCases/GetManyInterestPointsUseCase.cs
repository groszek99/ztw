using System.Collections.Generic;
using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.InterestPoints.UseCases;

public class GetManyInterestPointsUseCase : IUseCase<GetManyInterestPointsUseCase.IOutput> {
  private readonly IInterestPointsRepository interestPointsRepository;

  public GetManyInterestPointsUseCase(IInterestPointsRepository interestPointsRepository) =>
    this.interestPointsRepository = interestPointsRepository;

  public async Task Execute(NullInputPort input, IOutput output) {
    var interestPoint = await interestPointsRepository.GetManyAsync();

    output.Found(interestPoint);
  }

  public interface IOutput : IOutputPort {
    void Found(IReadOnlyCollection<InterestPoint> interestPoints);
  }
}