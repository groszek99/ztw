using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.InterestPoints.UseCases;

public class
  GetOneInterestPointUseCase : IUseCase<GetOneInterestPointUseCase.Input, GetOneInterestPointUseCase.IOutput> {
  private readonly IInterestPointsRepository interestPointsRepository;

  public GetOneInterestPointUseCase(IInterestPointsRepository interestPointsRepository) =>
    this.interestPointsRepository = interestPointsRepository;

  public async Task Execute(Input input, IOutput output) {
    var interestPoint = await interestPointsRepository.GetOneAsync(input.Identifier);

    if (interestPoint is null) {
      output.NotFound(input);

      return;
    }

    output.Found(interestPoint);
  }

  public record Input : IInputPort {
    public long Identifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Found(InterestPoint interestPoint);

    void NotFound(Input input);
  }
}