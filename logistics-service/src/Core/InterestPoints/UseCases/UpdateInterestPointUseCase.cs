using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.InterestPoints.UseCases;

public class
  UpdateInterestPointUseCase : IUseCase<UpdateInterestPointUseCase.Input, UpdateInterestPointUseCase.IOutput> {
  private readonly IInterestPointsRepository interestPointsRepository;

  public UpdateInterestPointUseCase(IInterestPointsRepository interestPointsRepository) =>
    this.interestPointsRepository = interestPointsRepository;

  public async Task Execute(Input input, IOutput output) {
    var interestPoint = await interestPointsRepository.GetOneAsync(input.Identifier);

    if (interestPoint is null) {
      output.NotFound(input);

      return;
    }

    var newInterestPoint = await interestPointsRepository.UpdateAsync(interestPoint, input);

    output.Updated(newInterestPoint);
  }

  public record Input : InterestPoint, IInputPort { }

  public interface IOutput : IOutputPort {
    void Updated(InterestPoint interestPoint);

    void NotFound(Input input);

    void OwnerNotFound(Input input);
  }
}