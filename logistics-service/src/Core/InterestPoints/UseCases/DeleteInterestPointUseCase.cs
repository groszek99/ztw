using System.Threading.Tasks;
using Logistics.Core.Abstraction;

namespace Logistics.Core.InterestPoints.UseCases;

public class
  DeleteInterestPointUseCase : IUseCase<DeleteInterestPointUseCase.Input, DeleteInterestPointUseCase.IOutput> {
  private readonly IInterestPointsRepository interestPointsRepository;

  public DeleteInterestPointUseCase(IInterestPointsRepository interestPointsRepository) =>
    this.interestPointsRepository = interestPointsRepository;

  public async Task Execute(Input input, IOutput output) {
    var interestPoint = await interestPointsRepository.GetOneAsync(input.Identifier);

    if (interestPoint is null) {
      output.NotFound(input);

      return;
    }

    await interestPointsRepository.DeleteAsync(interestPoint);

    output.Deleted(interestPoint);
  }

  public record Input : IInputPort {
    public long Identifier { get; init; }
  }

  public interface IOutput : IOutputPort {
    void Deleted(InterestPoint interestPoint);

    void NotFound(Input input);
  }
}