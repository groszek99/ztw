namespace Logistics.Core.Common;

public record Coordinates {
  public double Longitude { get; init; }
  public double Latitude { get; init; }
}