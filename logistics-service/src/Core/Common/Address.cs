namespace Logistics.Core.Common;

public record Address {
  public string? Apartment { get; init; }
  public string Building { get; init; }
  public string Street { get; init; }
  public string City { get; init; }
  public string ZipCode { get; init; }
  public string Country { get; init; }
}