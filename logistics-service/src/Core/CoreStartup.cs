using Logistics.Core.InterestPoints.UseCases;
using Microsoft.Extensions.DependencyInjection;

namespace Logistics.Core;

public static class CoreStartup {
  public static IServiceCollection AddCore(this IServiceCollection services) {
    // services.AddScoped<GetUsersUseCase>();
    // services.AddScoped<UpdateUserUseCase>();
    //
    // services.AddScoped<GetCalendarEventsUseCase>();
    // services.AddScoped<CreateCalendarEventUseCase>();
    // services.AddScoped<UpdateCalendarEventUseCase>();
    //
    // services.AddScoped<GetRootTasksUseCase>();
    // services.AddScoped<GetChildrenTasksUseCase>();
    // services.AddScoped<CreateTaskUseCase>();
    // services.AddScoped<UpdateTaskUseCase>();

    services.AddScoped<CreateInterestPointUseCase>();
    services.AddScoped<DeleteInterestPointUseCase>();
    services.AddScoped<GetManyInterestPointsUseCase>();
    services.AddScoped<GetOneInterestPointUseCase>();
    services.AddScoped<UpdateInterestPointUseCase>();

    return services;
  }
}