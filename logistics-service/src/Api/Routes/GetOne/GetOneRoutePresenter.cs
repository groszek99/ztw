using Logistics.Api.Abstraction;
using Logistics.Core.Routes;
using Logistics.Core.Routes.UseCases;

namespace Logistics.Api.Routes.GetOne;

public class GetOneRoutePresenter : ActionResultPresenter, GetOneRouteUseCase.IOutput {
  public void Found(Route route) {
    var dto = new RouteDto(route);

    Ok(dto);
  }

  public void NotFound(GetOneRouteUseCase.Input input) {
    NotFound();
  }
}