using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.Routes.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Routes.GetOne;

[ApiController]
public class GetOneRouteController : ControllerBase {
  private readonly GetOneRoutePresenter presenter;
  private readonly GetOneRouteUseCase useCase;

  public GetOneRouteController(GetOneRouteUseCase useCase, GetOneRoutePresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Gets interest point with given identifier
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <returns>Requested interest point</returns>
  /// <response code="200">If found interest point with given identifier</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpGet("api/v{version:apiVersion}/routes/{identifier:long}", Name = nameof(GetOneRoute))]
  [Produces(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(RouteDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  public async Task<IActionResult> GetOneRoute([FromRoute] long identifier) {
    var input = new GetOneRouteUseCase.Input {
      Identifier = identifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}