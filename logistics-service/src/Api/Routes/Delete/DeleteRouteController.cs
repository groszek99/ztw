using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.Routes.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Routes.Delete;

[ApiController]
public class DeleteRouteController : ControllerBase {
  private readonly DeleteRoutePresenter presenter;
  private readonly DeleteRouteUseCase useCase;

  public DeleteRouteController(DeleteRouteUseCase useCase, DeleteRoutePresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Deletes interest point with given identifier
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <returns>A deleted interest point</returns>
  /// <response code="200">If successfully deleted interest point</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpDelete("api/v{version:apiVersion}/routes/{identifier:long}", Name = nameof(DeleteRoute))]
  [Produces(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(RouteDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  public async Task<IActionResult> DeleteRoute([FromRoute] long identifier) {
    var input = new DeleteRouteUseCase.Input {
      Identifier = identifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}