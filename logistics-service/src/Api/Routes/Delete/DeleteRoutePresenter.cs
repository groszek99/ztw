using Logistics.Api.Abstraction;
using Logistics.Core.Routes;
using Logistics.Core.Routes.UseCases;

namespace Logistics.Api.Routes.Delete;

public class DeleteRoutePresenter : ActionResultPresenter, DeleteRouteUseCase.IOutput {
  public void Deleted(Route route) {
    var dto = new RouteDto(route);

    Ok(dto);
  }

  public void NotFound(DeleteRouteUseCase.Input input) {
    NotFound();
  }
}