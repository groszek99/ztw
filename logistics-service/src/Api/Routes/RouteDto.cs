using System.ComponentModel.DataAnnotations;
using Logistics.Core.Routes;

namespace Logistics.Api.Routes;

public record RouteDto {
  public RouteDto(Route route) {
    Identifier = route.Identifier;
    Name = route.Name;
    UserIdentifier = route.UserIdentifier;
  }

  [Required]
  public long Identifier { get; }

  [Required]
  public string Name { get; }

  [Required]
  public string UserIdentifier { get; }
}