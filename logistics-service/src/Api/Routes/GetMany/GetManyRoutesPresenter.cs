using System.Collections.Generic;
using System.Linq;
using Logistics.Api.Abstraction;
using Logistics.Core.Routes;
using Logistics.Core.Routes.UseCases;

namespace Logistics.Api.Routes.GetMany;

public class GetManyRoutesPresenter : ActionResultPresenter, GetManyRoutesUseCase.IOutput {
  public void Found(IReadOnlyCollection<Route> routes) {
    if (routes.Count == 0) {
      NoContent();
      return;
    }

    var dtos = routes.Select(route => new RouteDto(route))
      .ToList();
    Ok(dtos);
  }
}