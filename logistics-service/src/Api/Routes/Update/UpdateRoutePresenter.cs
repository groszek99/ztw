using Logistics.Api.Abstraction;
using Logistics.Core.Routes;
using Logistics.Core.Routes.UseCases;

namespace Logistics.Api.Routes.Update;

public class UpdateRoutePresenter : ActionResultPresenter, UpdateRouteUseCase.IOutput {
  public void Updated(Route route) {
    var dto = new RouteDto(route);
    Ok(dto);
  }

  public void NotFound(UpdateRouteUseCase.Input input) {
    NotFound();
  }

  public void OwnerNotFound(UpdateRouteUseCase.Input input) {
    ValidationProblem(nameof(UpdateRouteCommand.OwnerIdentifier),
      $"There is no user with identifier '{input.UserIdentifier}'.");
  }
}