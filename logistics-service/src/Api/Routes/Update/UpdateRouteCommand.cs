using System.ComponentModel.DataAnnotations;

namespace Logistics.Api.Routes.Update;

public record UpdateRouteCommand {
  [Required]
  public string Name { get; init; }

  [Required]
  public string OwnerIdentifier { get; init; }
}