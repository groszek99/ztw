using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.Routes.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Routes.Update;

[ApiController]
public class UpdateRouteController : ControllerBase {
  private readonly UpdateRoutePresenter presenter;
  private readonly UpdateRouteUseCase useCase;

  public UpdateRouteController(UpdateRouteUseCase useCase, UpdateRoutePresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Updates existing route
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <param name="command">New interest point data</param>
  /// <returns>A updated interest point</returns>
  /// <response code="200">If successfully updated interest point</response>
  /// <response code="400">If the command data is invalid</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpPut("api/v{version:apiVersion}/routes/{identifier:long}", Name = nameof(UpdateRoute))]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(RouteDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
  public async Task<IActionResult> UpdateRoute([FromRoute] long identifier, [FromBody] UpdateRouteCommand command) {
    var input = new UpdateRouteUseCase.Input {
      Identifier = identifier,
      Name = command.Name,
      UserIdentifier = command.OwnerIdentifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}