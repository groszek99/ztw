using System.ComponentModel.DataAnnotations;

namespace Logistics.Api.Routes.Create;

public record CreateRouteCommand {
  [Required]
  public string Name { get; init; }

  [Required]
  public string OwnerIdentifier { get; init; }
}