using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.Routes.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Routes.Create;

[ApiController]
public class CreateRouteController : ControllerBase {
  private readonly CreateRoutePresenter presenter;
  private readonly CreateRouteUseCase useCase;

  public CreateRouteController(CreateRouteUseCase useCase, CreateRoutePresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Creates a new route
  /// </summary>
  /// <param name="command">New interest point data</param>
  /// <returns>A newly created interest point</returns>
  /// <response code="201">If successfully created interest point</response>
  /// <response code="400">If the command data is invalid</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpPost("api/v{version:apiVersion}/routes", Name = nameof(CreateRoute))]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(RouteDto), StatusCodes.Status201Created)]
  [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
  public async Task<IActionResult> CreateRoute([FromBody] CreateRouteCommand command) {
    var input = new CreateRouteUseCase.Input {
      Name = command.Name,
      UserIdentifier = command.OwnerIdentifier,
    };

    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}