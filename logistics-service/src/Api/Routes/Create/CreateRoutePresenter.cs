using Logistics.Api.Abstraction;
using Logistics.Api.Routes.GetOne;
using Logistics.Core.Routes;
using Logistics.Core.Routes.UseCases;

namespace Logistics.Api.Routes.Create;

public class CreateRoutePresenter : ActionResultPresenter, CreateRouteUseCase.IOutput {
  public void Created(Route route) {
    var dto = new RouteDto(route);

    Created(nameof(GetOneRouteController.GetOneRoute), new {
      identifier = dto.Identifier,
    }, dto);
  }

  public void OwnerNotFound(CreateRouteUseCase.Input input) {
    ValidationProblem(nameof(CreateRouteCommand.OwnerIdentifier), "Unknown user identifier");
  }
}