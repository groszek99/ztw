using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Logistics.Api.Configuration;

/// <summary>
///   Configures the Swagger generation options.
/// </summary>
/// <remarks>
///   This allows API versioning to define a Swagger document per API version after the
///   <see cref="IApiVersionDescriptionProvider" /> service has been resolved from the service container.
/// </remarks>
public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions> {
  private readonly IApiVersionDescriptionProvider provider;

  /// <summary>
  ///   Initializes a new instance of the <see cref="ConfigureSwaggerOptions" /> class.
  /// </summary>
  /// <param name="provider">
  ///   The <see cref="IApiVersionDescriptionProvider">provider</see> used to generate Swagger
  ///   documents.
  /// </param>
  public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) => this.provider = provider;

  /// <inheritdoc />
  public void Configure(SwaggerGenOptions options) {
    options.TagActionsBy(api => new List<string> {
      ApiConstants.ApiName,
    });
    options.UseDateOnlyTimeOnlyStringConverters();

    options.UseOneOfForPolymorphism();
    options.UseAllOfForInheritance();

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

    foreach (var description in provider.ApiVersionDescriptions)
      options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));

    options.AddSecurityDefinition("Bearer", new() {
      In = ParameterLocation.Header,
      Description = "Please enter a valid token",
      Name = "Authorization",
      Type = SecuritySchemeType.Http,
      BearerFormat = "JWT",
      Scheme = "Bearer",
    });
    options.AddSecurityRequirement(new() {
      {
        new() {
          Reference = new() {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer",
          },
        },
        new string[] { }
      },
    });
  }

  private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description) {
    var info = new OpenApiInfo {
      Title = $"{ApiConstants.ApiName} {description.ApiVersion}",
      Version = description.ApiVersion.ToString(),
    };

    if (description.IsDeprecated) info.Description += " This API version has been deprecated.";

    return info;
  }
}