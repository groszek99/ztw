namespace Logistics.Api.Configuration;

public static class ApiConstants {
  public const string ApiName = "Logistics API";

  public static class Versions {
    public const string V1 = "1.0";
    public const string DefaultVersion = V1;
  }
}