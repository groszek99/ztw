using System.Text.Json;
using System.Text.Json.Serialization;
using Logistics.Api.Configuration;
using Logistics.Api.InterestPoints.Create;
using Logistics.Api.InterestPoints.Delete;
using Logistics.Api.InterestPoints.GetMany;
using Logistics.Api.InterestPoints.GetOne;
using Logistics.Api.InterestPoints.Update;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Logistics.Api;

public static class ApiStartup {
  private static IServiceCollection AddPresenters(this IServiceCollection services) =>
    services.AddScoped<CreateInterestPointPresenter>()
      .AddScoped<DeleteInterestPointPresenter>()
      .AddScoped<GetManyInterestPointsPresenter>()
      .AddScoped<GetOneInterestPointPresenter>()
      .AddScoped<UpdateInterestPointPresenter>();

  public static IServiceCollection AddApi(this IServiceCollection services) {
    services
      .AddControllers(options => options.UseDateOnlyTimeOnlyStringConverters())
      .AddJsonOptions(options => {
        options.UseDateOnlyTimeOnlyStringConverters();
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
      });

    services.AddApiVersioning(options => {
      options.ReportApiVersions = true;
      options.UseApiBehavior = true;
      options.AssumeDefaultVersionWhenUnspecified = true;
      options.DefaultApiVersion = ApiVersion.Parse(ApiConstants.Versions.DefaultVersion);
    });

    services.AddVersionedApiExplorer(options => {
      options.GroupNameFormat = "'v'VVV";
      options.SubstituteApiVersionInUrl = true;
    });

    services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

    services.AddSwaggerGen();
    services.AddPresenters();

    return services;
  }

  public static WebApplication UseApi(this WebApplication app) {
    var apiVersionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();

    app.UseSwagger();
    app.UseSwaggerUI(options => {
      foreach (var description in apiVersionProvider.ApiVersionDescriptions) {
        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
          description.GroupName.ToUpperInvariant());
      }

      options.DisplayOperationId();
    });

    app.UseSerilogRequestLogging();

    return app;
  }

  public static IEndpointRouteBuilder MapApiEndpoints(this IEndpointRouteBuilder endpoints) {
    endpoints.MapControllers();

    return endpoints;
  }
}