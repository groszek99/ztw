using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Logistics.Core.Common;

namespace Logistics.Api.Common;

public record CoordinatesDto {
  public CoordinatesDto(Coordinates coordinates) {
    Latitude = coordinates.Latitude;
    Longitude = coordinates.Longitude;
  }

  [JsonConstructor]
  public CoordinatesDto() { }

  [Required]
  public double Latitude { get; init; }

  [Required]
  public double Longitude { get; init; }

  public Coordinates ToDomain() =>
    new() {
      Latitude = Latitude,
      Longitude = Longitude,
    };
}