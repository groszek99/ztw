using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Logistics.Core.Common;

namespace Logistics.Api.Common;

public record AddressDto {
  public AddressDto(Address address) {
    Apartment = address.Apartment;
    Building = address.Building;
    Street = address.Street;
    City = address.City;
    ZipCode = address.ZipCode;
    Country = address.Country;
  }

  [JsonConstructor]
  public AddressDto() { }

  [DefaultValue(null)]
  public string? Apartment { get; init; }

  [Required]
  public string Building { get; init; }

  [Required]
  public string Street { get; init; }

  [Required]
  public string City { get; init; }

  [Required]
  public string ZipCode { get; init; }

  [Required]
  public string Country { get; init; }

  public Address ToDomain() =>
    new() {
      Apartment = Apartment,
      Building = Building,
      City = City,
      Country = Country,
      Street = Street,
      ZipCode = ZipCode,
    };
}