using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.Abstraction;

public abstract class ActionResultPresenter {
  private readonly TaskCompletionSource<IActionResult> resultSource = new();

  protected void Ok(object? value) {
    resultSource.SetResult(new OkObjectResult(value));
  }

  protected void Created(string? routeName, object? routeValues, object? value) {
    resultSource.SetResult(new CreatedAtRouteResult(routeName, routeValues, value));
  }

  protected void NoContent() {
    resultSource.SetResult(new NoContentResult());
  }

  protected void NotFound() {
    resultSource.SetResult(new NotFoundResult());
  }

  protected void Problem(string title, string detail) {
    resultSource.SetResult(new BadRequestObjectResult(new ProblemDetails {
      Type = "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.1",
      Title = title,
      Detail = detail,
    }));
  }

  protected void ValidationProblem(string fieldName, string errorMessage) {
    resultSource.SetResult(new BadRequestObjectResult(new ValidationProblemDetails(new Dictionary<string, string[]> {
      [fieldName] = new[] {
        errorMessage,
      },
    }) {
      Type = "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.1",
    }));
  }

  public Task<IActionResult> GetResultAsync() => resultSource.Task;
}