using System.ComponentModel.DataAnnotations;
using Logistics.Api.Common;
using Logistics.Core.InterestPoints;

namespace Logistics.Api.InterestPoints;

public record InterestPointDto {
  public InterestPointDto(InterestPoint interestPoint) {
    Identifier = interestPoint.Identifier;
    Name = interestPoint.Name;
    Address = new(interestPoint.Address);
    UserIdentifier = interestPoint.UserIdentifier;
  }

  [Required]
  public long Identifier { get; }

  [Required]
  public string Name { get; }

  [Required]
  public AddressDto Address { get; }

  [Required]
  public string UserIdentifier { get; }
}