using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.InterestPoints.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.InterestPoints.GetOne;

[ApiController]
public class GetOneInterestPointController : ControllerBase {
  private readonly GetOneInterestPointPresenter presenter;
  private readonly GetOneInterestPointUseCase useCase;

  public GetOneInterestPointController(GetOneInterestPointUseCase useCase, GetOneInterestPointPresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Gets interest point with given identifier
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <returns>Requested interest point</returns>
  /// <response code="200">If found interest point with given identifier</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpGet("api/v{version:apiVersion}/interest-points/{identifier:long}", Name = nameof(GetOneInterestPoint))]
  [Produces(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(InterestPointDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  public async Task<IActionResult> GetOneInterestPoint([FromRoute] long identifier) {
    var input = new GetOneInterestPointUseCase.Input {
      Identifier = identifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}