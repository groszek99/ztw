using Logistics.Api.Abstraction;
using Logistics.Core.InterestPoints;
using Logistics.Core.InterestPoints.UseCases;

namespace Logistics.Api.InterestPoints.GetOne;

public class GetOneInterestPointPresenter : ActionResultPresenter, GetOneInterestPointUseCase.IOutput {
  public void Found(InterestPoint interestPoint) {
    var dto = new InterestPointDto(interestPoint);

    Ok(dto);
  }

  public void NotFound(GetOneInterestPointUseCase.Input input) {
    NotFound();
  }
}