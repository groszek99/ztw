using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.InterestPoints.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.InterestPoints.Create;

[ApiController]
public class CreateInterestPointController : ControllerBase {
  private readonly CreateInterestPointPresenter presenter;
  private readonly CreateInterestPointUseCase useCase;

  public CreateInterestPointController(CreateInterestPointUseCase useCase, CreateInterestPointPresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Creates a new interestPoint
  /// </summary>
  /// <param name="command">New interest point data</param>
  /// <returns>A newly created interest point</returns>
  /// <response code="201">If successfully created interest point</response>
  /// <response code="400">If the command data is invalid</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpPost("api/v{version:apiVersion}/interest-points", Name = nameof(CreateInterestPoint))]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(InterestPointDto), StatusCodes.Status201Created)]
  [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
  public async Task<IActionResult> CreateInterestPoint([FromBody] CreateInterestPointCommand command) {
    var input = new CreateInterestPointUseCase.Input {
      Address = command.Address.ToDomain(),
      Name = command.Name,
      Coordinates = command.Coordinates.ToDomain(),
      UserIdentifier = command.OwnerIdentifier,
    };

    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}