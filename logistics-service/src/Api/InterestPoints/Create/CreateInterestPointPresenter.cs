using Logistics.Api.Abstraction;
using Logistics.Api.InterestPoints.GetOne;
using Logistics.Core.InterestPoints;
using Logistics.Core.InterestPoints.UseCases;

namespace Logistics.Api.InterestPoints.Create;

public class CreateInterestPointPresenter : ActionResultPresenter, CreateInterestPointUseCase.IOutput {
  public void Created(InterestPoint interestPoint) {
    var dto = new InterestPointDto(interestPoint);

    Created(nameof(GetOneInterestPointController.GetOneInterestPoint), new {
      identifier = dto.Identifier,
    }, dto);
  }

  public void OwnerNotFound(CreateInterestPointUseCase.Input input) {
    ValidationProblem(nameof(CreateInterestPointCommand.OwnerIdentifier), "Unknown user identifier");
  }
}