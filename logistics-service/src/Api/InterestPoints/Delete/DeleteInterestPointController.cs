using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.InterestPoints.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.InterestPoints.Delete;

[ApiController]
public class DeleteInterestPointController : ControllerBase {
  private readonly DeleteInterestPointPresenter presenter;
  private readonly DeleteInterestPointUseCase useCase;

  public DeleteInterestPointController(DeleteInterestPointUseCase useCase, DeleteInterestPointPresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Deletes interest point with given identifier
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <returns>A deleted interest point</returns>
  /// <response code="200">If successfully deleted interest point</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpDelete("api/v{version:apiVersion}/interest-points/{identifier:long}", Name = nameof(DeleteInterestPoint))]
  [Produces(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(InterestPointDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  public async Task<IActionResult> DeleteInterestPoint([FromRoute] long identifier) {
    var input = new DeleteInterestPointUseCase.Input {
      Identifier = identifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}