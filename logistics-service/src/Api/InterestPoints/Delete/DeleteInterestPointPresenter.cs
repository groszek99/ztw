using Logistics.Api.Abstraction;
using Logistics.Core.InterestPoints;
using Logistics.Core.InterestPoints.UseCases;

namespace Logistics.Api.InterestPoints.Delete;

public class DeleteInterestPointPresenter : ActionResultPresenter, DeleteInterestPointUseCase.IOutput {
  public void Deleted(InterestPoint interestPoint) {
    var dto = new InterestPointDto(interestPoint);

    Ok(dto);
  }

  public void NotFound(DeleteInterestPointUseCase.Input input) {
    NotFound();
  }
}