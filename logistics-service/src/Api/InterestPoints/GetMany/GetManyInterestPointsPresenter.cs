using System.Collections.Generic;
using System.Linq;
using Logistics.Api.Abstraction;
using Logistics.Core.InterestPoints;
using Logistics.Core.InterestPoints.UseCases;

namespace Logistics.Api.InterestPoints.GetMany;

public class GetManyInterestPointsPresenter : ActionResultPresenter, GetManyInterestPointsUseCase.IOutput {
  public void Found(IReadOnlyCollection<InterestPoint> interestPoints) {
    if (interestPoints.Count == 0) {
      NoContent();
      return;
    }

    var dtos = interestPoints.Select(interestPoint => new InterestPointDto(interestPoint))
      .ToList();
    Ok(dtos);
  }
}