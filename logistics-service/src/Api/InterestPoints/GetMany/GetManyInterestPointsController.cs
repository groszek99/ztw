using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.Abstraction;
using Logistics.Core.InterestPoints.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.InterestPoints.GetMany;

[ApiController]
public class GetManyInterestPointsController : ControllerBase {
  private readonly GetManyInterestPointsPresenter presenter;
  private readonly GetManyInterestPointsUseCase useCase;

  public GetManyInterestPointsController(GetManyInterestPointsUseCase useCase,
    GetManyInterestPointsPresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Gets interest point collection
  /// </summary>
  /// <returns>A interest point list</returns>
  /// <response code="200">If there are any interest point</response>
  /// <response code="204">If there are no interest point</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpGet("api/v{version:apiVersion}/interest-points", Name = nameof(GetManyInterestPoints))]
  [Produces(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(IEnumerable<InterestPointDto>), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(void), StatusCodes.Status204NoContent)]
  [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
  [ProducesResponseType(typeof(void), StatusCodes.Status403Forbidden)]
  public async Task<IActionResult> GetManyInterestPoints() {
    await useCase.Execute(NullInputPort.Instance, presenter);

    return await presenter.GetResultAsync();
  }
}