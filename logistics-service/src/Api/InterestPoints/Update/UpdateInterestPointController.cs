using System.Net.Mime;
using System.Threading.Tasks;
using Logistics.Api.Configuration;
using Logistics.Core.InterestPoints.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Logistics.Api.InterestPoints.Update;

[ApiController]
public class UpdateInterestPointController : ControllerBase {
  private readonly UpdateInterestPointPresenter presenter;
  private readonly UpdateInterestPointUseCase useCase;

  public UpdateInterestPointController(UpdateInterestPointUseCase useCase, UpdateInterestPointPresenter presenter) {
    this.useCase = useCase;
    this.presenter = presenter;
  }

  /// <summary>
  ///   Updates existing interestPoint
  /// </summary>
  /// <param name="identifier">Interest point identifier</param>
  /// <param name="command">New interest point data</param>
  /// <returns>A updated interest point</returns>
  /// <response code="200">If successfully updated interest point</response>
  /// <response code="400">If the command data is invalid</response>
  /// <response code="404">If no interest point with given identifier</response>
  [Authorize]
  [AllowAnonymous]
  [ApiVersion(ApiConstants.Versions.V1)]
  [HttpPut("api/v{version:apiVersion}/interest-points/{identifier:long}", Name = nameof(UpdateInterestPoint))]
  [Produces(MediaTypeNames.Application.Json)]
  [Consumes(MediaTypeNames.Application.Json)]
  [ProducesResponseType(typeof(InterestPointDto), StatusCodes.Status200OK)]
  [ProducesResponseType(typeof(ProblemDetails), StatusCodes.Status404NotFound)]
  [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
  public async Task<IActionResult> UpdateInterestPoint([FromRoute] long identifier,
    [FromBody] UpdateInterestPointCommand command) {
    var input = new UpdateInterestPointUseCase.Input {
      Identifier = identifier,
      Address = command.Address.ToDomain(),
      Name = command.Name,
      Coordinates = command.Coordinates.ToDomain(),
      UserIdentifier = command.OwnerIdentifier,
    };
    await useCase.Execute(input, presenter);

    return await presenter.GetResultAsync();
  }
}