using Logistics.Api.Abstraction;
using Logistics.Core.InterestPoints;
using Logistics.Core.InterestPoints.UseCases;

namespace Logistics.Api.InterestPoints.Update;

public class UpdateInterestPointPresenter : ActionResultPresenter, UpdateInterestPointUseCase.IOutput {
  public void Updated(InterestPoint interestPoint) {
    var dto = new InterestPointDto(interestPoint);
    Ok(dto);
  }

  public void NotFound(UpdateInterestPointUseCase.Input input) {
    NotFound();
  }

  public void OwnerNotFound(UpdateInterestPointUseCase.Input input) {
    ValidationProblem(nameof(UpdateInterestPointCommand.OwnerIdentifier),
      $"There is no user with identifier '{input.UserIdentifier}'.");
  }
}