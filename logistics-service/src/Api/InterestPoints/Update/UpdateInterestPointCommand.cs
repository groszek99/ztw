using System.ComponentModel.DataAnnotations;
using Logistics.Api.Common;

namespace Logistics.Api.InterestPoints.Update;

public record UpdateInterestPointCommand {
  [Required]
  public string Name { get; init; }

  [Required]
  public AddressDto Address { get; init; }

  [Required]
  public CoordinatesDto Coordinates { get; init; }

  [Required]
  public string OwnerIdentifier { get; init; }
}