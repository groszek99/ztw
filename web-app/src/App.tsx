import { HopeProvider, HopeThemeConfig } from '@hope-ui/solid';
import axios from 'axios';
import { AuthGuard } from './components/guards/AuthGuard';
import { MapView } from './views/MapView';

axios.defaults.baseURL = '/';
axios.interceptors.response.use((response) => {
  if (response.status == 204) {
    return { ...response, data: [] };
  }
  return response;
});

const config: HopeThemeConfig = {
  initialColorMode: 'system',
};

export function App() {
  return (
    <HopeProvider config={config}>
      <AuthGuard>
        <MapView />
      </AuthGuard>
    </HopeProvider>
  );
}
