import { Box, HStack, VStack } from '@hope-ui/solid';
// import 'mapbox-gl';
import { createSignal } from 'solid-js';
import type { Viewport } from 'solid-map-gl';
import MapGL, { Layer, Marker, Source } from 'solid-map-gl';
import { InterestPointsList } from '../components/InterestPointsList';
import { RoutesList } from '../components/RoutesList';
import { MAPBOX_ACCESS_TOKEN } from '../constants';

const data = {
  type: 'Feature',
  geometry: {
    type: 'LineString',
    coordinates: [
      [-122.48369693756104, 37.83381888486939],
      [-122.48348236083984, 37.83317489144141],
      [-122.48339653015138, 37.83270036637107],
      [-122.48356819152832, 37.832056363179625],
      [-122.48404026031496, 37.83114119107971],
      [-122.48404026031496, 37.83049717427869],
      [-122.48348236083984, 37.829920943955045],
      [-122.48356819152832, 37.82954808664175],
      [-122.48507022857666, 37.82944639795659],
      [-122.48610019683838, 37.82880236636284],
      [-122.48695850372314, 37.82931081282506],
      [-122.48700141906738, 37.83080223556934],
      [-122.48751640319824, 37.83168351665737],
      [-122.48803138732912, 37.832158048267786],
      [-122.48888969421387, 37.83297152392784],
      [-122.48987674713133, 37.83263257682617],
      [-122.49043464660643, 37.832937629287755],
      [-122.49125003814696, 37.832429207817725],
      [-122.49163627624512, 37.832564787218985],
      [-122.49223709106445, 37.83337825839438],
      [-122.49378204345702, 37.83368330777276],
    ],
  },
};

export function MapView() {
  const [viewport, setViewport] = createSignal<Viewport>({
    center: [-122.41, 37.78],
    zoom: 11,
  });

  return (
    <HStack width="100%" height="100%" wrap="nowrap">
      <VStack height="100%" width={200}>
        <RoutesList />
      </VStack>
      <Box height="100%" flex={1} position="relative">
        <MapGL
          options={{
            accessToken: MAPBOX_ACCESS_TOKEN,
            style: 'mapbox://styles/mapbox/streets-v11',
          }}
          viewport={viewport()}
          onViewportChange={(evt) => setViewport(evt)}
        >
          <Marker lngLat={[-122.41, 37.78]} />
          <Marker lngLat={[-122.31, 37.78]} options={{ draggable: true }} />
          <Source
            source={{
              type: 'geojson',
              data: data,
            }}
          >
            <Layer
              style={{
                type: 'line',
                layout: {
                  'line-join': 'round',
                  'line-cap': 'round',
                },
                paint: {
                  'line-color': '#888',
                  'line-width': 8,
                },
              }}
            />
          </Source>
        </MapGL>
      </Box>
      <VStack height="100%" width={200}>
        <InterestPointsList />
      </VStack>
    </HStack>
  );
}
