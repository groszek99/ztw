import { DirectionsWaypoint, type MapboxProfile } from '@mapbox/mapbox-sdk/lib/classes/mapi-request';
import Directions from '@mapbox/mapbox-sdk/services/directions';
import { MAPBOX_ACCESS_TOKEN } from '../constants';

const directions = Directions({
  accessToken: MAPBOX_ACCESS_TOKEN,
});

export interface FetchRouteOptions {
  profile: MapboxProfile;
  waypoints: DirectionsWaypoint[];
}

export async function fetchRoute(options: FetchRouteOptions) {
  const request = directions.getDirections({
    geometries: 'geojson',
    profile: options.profile,
    alternatives: 'false',
    annotations: ['speed', 'distance'],
    continueStraight: true,
    waypoints: options.waypoints,
  });

  const response = await request.send();

  return response.body;
}
