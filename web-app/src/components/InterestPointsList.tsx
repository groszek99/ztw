import { Badge, Divider, IconButton, List, ListItem, Text } from '@hope-ui/solid';
import { RiMapMapPinAddFill } from 'solid-icons/ri';

export function InterestPointsList() {
  return (
    <List padding={8} width="100%" spacing="$2">
      <ListItem>
        <Badge>Name</Badge>
        <Text size="sm">Lon, Lat</Text>
      </ListItem>
      <Divider />
      <ListItem>
        <Badge>Name</Badge>
        <Text size="sm">Lon, Lat</Text>
      </ListItem>
      <Divider />
      <ListItem>
        <Badge>Name</Badge>
        <Text size="sm">Lon, Lat</Text>
      </ListItem>
      <Divider />
      <ListItem>
        <Badge>Name</Badge>
        <Text size="sm">Lon, Lat</Text>
      </ListItem>
      <Divider />
      <ListItem>
        <Badge>Name</Badge>
        <Text size="sm">Lon, Lat</Text>
      </ListItem>

      <ListItem display={'inline-flex'} width={'100%'} justifyContent={'center'}>
        <IconButton aria-label="New point" size={'sm'} margin={'auto'} icon={<RiMapMapPinAddFill />} />
      </ListItem>
    </List>
  );
}
