import { Layer, Source } from 'solid-map-gl';

export function VehiclesLayer() {
  return (
    <Source
      source={{
        type: 'geojson',
        data: 'https://docs.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson',
      }}
    >
      <Layer
        style={{
          type: 'circle',
          source: 'earthquakes',
          paint: {
            'circle-radius': 8,
            'circle-color': 'red',
          },
        }}
      />
    </Source>
  );
}
