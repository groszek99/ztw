import { Center, Spinner } from '@hope-ui/solid';
import axios from 'axios';
import { children as resolveChildren, createEffect, createResource, JSX } from 'solid-js';

async function fetchUser() {
  try {
    const request = await axios.get<Record<string, string>>('/auth/user', {
      withCredentials: true,
    });

    if (request.status != 200) return undefined;

    return request.data;
  } catch (error) {
    console.error(error);
    return undefined;
  }
}

export interface AuthGuardProps {
  children: JSX.Element;
}

export function AuthGuard(props: AuthGuardProps) {
  const children = resolveChildren(() => props.children);
  const [user] = createResource(fetchUser);

  createEffect(() => {
    if (user.loading === false && user() === undefined) {
      window.location.href = '/auth/login?returnUrl=/';
    }
  });

  return (
    <>
      {user.error && <Center>CANNOT AUTHORIZE</Center>}
      {user.loading && (
        <Center width="100%" height="100%">
          <Spinner size="xl" />
        </Center>
      )}
      {user() !== undefined && children()}
    </>
  );
}
