using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ZTW.Gateway.Controllers;

[ApiController]
public class LoginController : ControllerBase {
  [HttpGet("/auth/login", Name = nameof(LoginUser))]
  public async Task<ActionResult> LoginUser([FromQuery] string returnUrl = "/") =>
    new ChallengeResult("Auth0", new() {
      RedirectUri = returnUrl,
    });
}