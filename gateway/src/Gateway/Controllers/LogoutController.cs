using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ZTW.Gateway.Controllers;

[ApiController]
public class LogoutController : ControllerBase {
  [Authorize]
  [HttpGet("/auth/logout", Name = nameof(LogoutUser))]
  public async Task<ActionResult> LogoutUser() {
    await HttpContext.SignOutAsync();

    return new SignOutResult("Auth0", new() {
      RedirectUri = "/",
    });
  }
}