using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ZTW.Gateway.Controllers;

[ApiController]
public class UserController : ControllerBase {
  [Authorize]
  [HttpGet("/auth/user", Name = nameof(GetUser))]
  public async Task<ActionResult> GetUser() {
    if (User.Identity?.IsAuthenticated == true) {
      var claims = ((ClaimsIdentity)User.Identity).Claims.Select(c =>
          new {
            type = c.Type,
            value = c.Value,
          })
        .ToList();

      return Ok(claims);
    }

    return BadRequest();
  }
}