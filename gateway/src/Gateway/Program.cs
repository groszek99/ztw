using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using Yarp.ReverseProxy.Transforms;

const string DefaultCorsPolicy = nameof(DefaultCorsPolicy);

Log.Logger = new LoggerConfiguration()
  .MinimumLevel.Debug()
  .Enrich.FromLogContext()
  .WriteTo.Console(
    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{Exception}{NewLine}",
    theme: AnsiConsoleTheme.Literate)
  .CreateBootstrapLogger();

Log.Information("Starting up");

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, services, configuration) =>
  configuration
    .MinimumLevel.Debug()
    .Enrich.FromLogContext()
    .WriteTo.Console(
      outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {SourceContext}{NewLine}\t{Message:lj}{Exception}{NewLine}",
      theme: AnsiConsoleTheme.Literate)
    .ReadFrom.Services(services)
    .ReadFrom.Configuration(context.Configuration));

builder.Host.ConfigureAppConfiguration((hostingContext, config) =>
  config.AddJsonFile("appsettings.local.json", true, true));

builder.Services.AddControllers();
builder.Services.AddCors(options => {
  options.AddDefaultPolicy(builder => builder.AllowAnyHeader()
    .AllowAnyMethod()
    .WithOrigins("https://localhost:5000", "http://localhost:3000"));
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAuthentication(options => {
    options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
  })
  .AddCookie(options => {
    options.Cookie.Name = "__BFF";
    options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
    options.Cookie.SameSite = SameSiteMode.Lax;
    options.Cookie.HttpOnly = true;
    options.LoginPath = "/auth/login";
    options.LogoutPath = "/auth/login";
  })
  .AddOpenIdConnect("Auth0", options => {
    options.Authority = $"https://{builder.Configuration["Auth0:Domain"]}";

    options.ClientId = builder.Configuration["Auth0:ClientId"];
    options.ClientSecret = builder.Configuration["Auth0:ClientSecret"];

    options.ResponseType = OpenIdConnectResponseType.CodeIdToken;
    options.ResponseMode = OpenIdConnectResponseMode.FormPost;

    options.Scope.Clear();
    options.Scope.Add("openid");
    options.Scope.Add("profile");
    options.Scope.Add("offline_access");
    options.Scope.Add("email");

    options.CallbackPath = new("/auth/login/callback");

    options.ClaimsIssuer = "Auth0";

    options.SaveTokens = true;
    options.MapInboundClaims = false;
    options.GetClaimsFromUserInfoEndpoint = true;

    options.Events = new() {
      // handle the logout redirection
      OnRedirectToIdentityProviderForSignOut = context => {
        var logoutUri =
          $"https://{builder.Configuration["Auth0:Domain"]}/v2/logout?client_id={builder.Configuration["Auth0:ClientId"]}";

        var postLogoutUri = context.Properties.RedirectUri;

        if (!string.IsNullOrEmpty(postLogoutUri)) {
          if (postLogoutUri.StartsWith("/")) {
            // transform to absolute
            var request = context.Request;
            postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase + postLogoutUri;
          }

          logoutUri += $"&returnTo={Uri.EscapeDataString(postLogoutUri)}";
        }

        context.Response.Redirect(logoutUri);
        context.HandleResponse();

        return Task.CompletedTask;
      },
      OnRedirectToIdentityProvider = context => {
        context.ProtocolMessage.SetParameter("audience", builder.Configuration["Auth0:ApiAudience"]);

        return Task.CompletedTask;
      },
    };
  });

builder.Services.AddHttpClient();

builder.Services.AddReverseProxy()
  .LoadFromConfig(builder.Configuration.GetSection("ReverseProxy"))
  .AddTransforms(builderContext => {
    builderContext.AddRequestTransform(async transformContext => {
      var accessToken = await transformContext.HttpContext.GetTokenAsync("access_token");

      if (accessToken != null) transformContext.ProxyRequest.Headers.Authorization = new("Bearer", accessToken);
    });
  });

var app = builder.Build();

if (app.Environment.IsDevelopment()) {
  app.UseDeveloperExceptionPage();
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseCors(DefaultCorsPolicy);
app.UseRouting();
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints => {
  endpoints.MapControllers();
  endpoints.MapReverseProxy();
});

app.Run();