export default {
  api: {
    input: {
      target: './swagger.json',
    },
    output: {
      workspace: 'src',
      mode: 'split',
      client: 'axios-functions',
      target: 'api.ts',
      schemas: 'models',
      prettier: true,
      clean: true,
      override: {
        useDates: true,
        mutator: {
          path: '../custom-instance.ts',
          name: 'customInstance',
        },
      },
    },
  },
};
